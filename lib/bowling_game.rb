class BowlingGame

  def initialize
    @rolls  = []
  end

  def roll(pins)
    @rolls << pins
  end

  def score

    score = 0
    first_in_frame = 0 
    10.times do |frame|

      if is_strike(first_in_frame)
        score += score_for_strike(first_in_frame)
        first_in_frame += 1
      else 
        if is_spare(first_in_frame)
          score += score_for_spare(first_in_frame)
        else
          score += frame_score(first_in_frame)
        end
        first_in_frame += 2
      end
      
    end

    score
  end

  private

  def is_spare(first_in_frame)
    frame_score(first_in_frame) == 10
  end

  def is_strike(first_in_frame)
    @rolls[first_in_frame] == 10
  end

  def frame_score(first_in_frame)
    @rolls[first_in_frame] + @rolls[first_in_frame+1]
  end

  def score_for_spare(first_in_frame)
    10 + @rolls[first_in_frame+2]
  end

  def score_for_strike(first_in_frame)
    10 + @rolls[first_in_frame+1] + @rolls[first_in_frame+2]
  end

end