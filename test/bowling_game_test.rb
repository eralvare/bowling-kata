require "test/unit"
require_relative "../lib/bowling_game"

class TestBowlingGame < Test::Unit::TestCase

  def setup
    @game = BowlingGame.new
  end

  def test_all_rolls_missed
    roll(20, 0)
    assert_equal(0, @game.score)
  end

  def test_all_ones
    roll(20, 1)
    assert_equal(20, @game.score)
  end

  def test_one_spare
    roll_spare
    @game.roll(3)
    roll(17, 0)

    assert_equal(16, @game.score)
  end

  def test_one_strike
    roll_strike
    @game.roll(4)
    @game.roll(3)
    roll(16, 0)

    assert_equal(24, @game.score)
  end

  def perfect_game
    roll(12,10)
    assert_equal(300, @game.score)
  end

  private

  def roll(n, pins)
    n.times { @game.roll pins}
  end

  def roll_spare
    @game.roll(5)
    @game.roll(5)    
  end

  def roll_strike
    @game.roll(10)
  end

end